const todoModel = (id: number, title: string, completed: boolean = false) => {
    return {
        id, // equivalent to id: id
         title, 
         completed
    }
};

export default todoModel;
