import TodoModel from './todoModel'

// input is an array of strings, output an array of TodoModel objects
const todoCollection = (...titleArr: string[]) => {
    let collection: object[] = titleArr.map((title, index) => {
        return TodoModel(index, title);
    });

    const get = (): object[] => {
        return collection
    };

    const add = (title: string): void => {
        const id = collection.length;
        // add model to collection using spread operator
        collection = [...collection, TodoModel(id, title)];
    };

    const remove = (id: number): void => {
        // use filter method because it is immutable
        // this creates a new array which will only contain items which are not equal
        // to id passed in as paramter
        collection = collection.filter((todoModel) => {
            return todoModel['id'] !== id;
        });
    };

    const toggleCompleted = (id: number): void => {
        // map is immutable - create a new array with model.completed property is reversed
        collection = collection.map((todoModel) => {
            if (todoModel['id'] === id) {
                return {
                    ...todoModel,
                    completed: !todoModel['completed']
                }
            }

            return todoModel;
        });
    };

    return {
        get,
        add,
        remove,
        toggleCompleted
    }
};

export default todoCollection;
