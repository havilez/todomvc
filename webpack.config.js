const path = require('path');
var cwd = process.cwd();

module.exports = {
    
    entry: './src/index.ts',
    output: {
        // path: __dirname + "/dist",
         path: path.resolve(__dirname, "dist"), // string
        // the target directory for all output files
        // must be an absolute path (use the Node.js path module)
    
        filename: 'bundle.js'
    },
    resolve: {
      extensions: ['.ts', '.js']
    },
    devtool: 'sourcemap',
    devServer: {
        port: 4000,
        contentBase: cwd,
        inline: true,
        hot: true,
        open: true
        
      },
    module: {
        // array of loaders
        rules: [
            {
                test: /\.ts?$/, loader: 'ts-loader'
            }
        ]
    },
    
};